package br.com.dontech.bfcao

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.MenuItem
import br.com.dontech.bfcao.databinding.ActivityAtualizarContaBinding

class AtualizarCadastroActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityAtualizarContaBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        /*binding.botaoAtualizarCadastro.setOnClickListener() {

            val user = DatabaseManeger.getUserDAO().findAll()[0]

            try {
                Thread {
                    UserService.saveUser(user)
                    runOnUiThread { finish() }

                }.start()
            }catch (ex: Exception){
                DatabaseManeger.getUserDAO().insert(user)
            }
        } */

        // colocar toolbar
        setSupportActionBar(binding.toolbarInclude.toolbar)

        supportActionBar?.title = "Criar conta"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home){
            finish()
        }
        return true
    }
}