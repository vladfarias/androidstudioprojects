package br.com.dontech.bfcao
import android.app.Application
import android.content.Context

class BFCaoApplication: Application() {


    override fun onCreate() {
        super.onCreate()
        appInstance = this
    }

    companion object {

        private var appInstance: BFCaoApplication? = null

        fun getInstance(): BFCaoApplication {
            if (appInstance == null) {
                throw IllegalAccessException("Configurar application no manifest")
            }
            return appInstance!!
        }
    }
}