package br.com.dontech.bfcao

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities =
[
       Passeador::class,
       User::class
]
, version = 1)

abstract class BFCaoDatabase : RoomDatabase() {
       abstract fun passeadorDAO(): PasseadorDAO
       abstract fun userDAO(): UserDAO

}