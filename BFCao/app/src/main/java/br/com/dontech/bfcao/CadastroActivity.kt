package br.com.dontech.bfcao

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import br.com.dontech.bfcao.databinding.CadastroBinding

class CadastroActivity : AppCompatActivity() {

    private val binding by lazy {
        CadastroBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.botaoCadastrar.setOnClickListener() {
            val user = User()
            user.name = binding.etNameUser.text.toString()
            user.email = binding.etEmailUser.text.toString()
            user.phone = binding.etPhoneUser.text.toString().toIntOrNull()
            user.lastname = binding.etlastNameUser.text.toString()
            user.password = binding.etPasswordUser.text.toString()

            //try {
                Thread {
                    UserService.saveUser(user)
                    runOnUiThread { finish() }

                }.start()
        //    } catch (ex: Exception) {
           //     DatabaseManeger.getUserDAO().insert()
        //    }
        }

        // colocar toolbar
        setSupportActionBar(binding.toolbarInclude.toolbar)

        supportActionBar?.title = "Criar conta"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            finish()
        }
        return true
    }
}