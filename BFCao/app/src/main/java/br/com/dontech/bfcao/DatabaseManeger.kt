package br.com.dontech.bfcao

import androidx.room.Room

object DatabaseManeger {

    private var dbInstance: BFCaoDatabase

    init {
        val contexto = BFCaoApplication.getInstance().applicationContext
        dbInstance = Room.databaseBuilder(contexto,
            BFCaoDatabase::class.java,
            "bfcao.sqlite").build()
    }

    fun getPasseadorDAO():PasseadorDAO{
        return dbInstance.passeadorDAO()
    }

    fun getUserDAO():UserDAO{
        return dbInstance.userDAO()
    }

}