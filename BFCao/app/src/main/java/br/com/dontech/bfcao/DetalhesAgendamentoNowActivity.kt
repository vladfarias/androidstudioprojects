package br.com.dontech.bfcao

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import br.com.dontech.bfcao.databinding.ActivityDetalhesAgendamentoNowBinding
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class DetalhesAgendamentoNowActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityDetalhesAgendamentoNowBinding.inflate(layoutInflater)
    }
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportActionBar?.title = "Agendamento"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        var p = intent.extras?.getSerializable("passeador") as Passeador
        binding.nomePasseador.text = "Nome: " + p.name
        binding.avalicaoPasseador.text = "Nota: " + p.rate.toString()
        binding.numeroPasseios.text = p.rides.toString() + " passeios realizados "
        var valor: Float? = 0F
        var duracao: Int? = 0
        var data: String? = ""
        var horario: String? = ""


        binding.image15minClaro.setVisibility(View.VISIBLE)
        binding.image15minEscuro.setVisibility(View.GONE)
        binding.image30minClaro.setVisibility(View.VISIBLE)
        binding.image30minEscuro.setVisibility(View.GONE)
        binding.image60minClaro.setVisibility(View.VISIBLE)
        binding.image60minEscuro.setVisibility(View.GONE)

        binding.imageRButtonAgoraFalse.setVisibility(View.VISIBLE)
        binding.imageRButtonAgoraTrue.setVisibility(View.GONE)
        binding.imageRButtonAgendarFalse.setVisibility(View.VISIBLE)
        binding.imageRButtonAgendarTrue.setVisibility(View.GONE)
        binding.camposDataHora.setVisibility(View.GONE)

        binding.image15minClaro.setOnClickListener{
            binding.image15minClaro.setVisibility(View.GONE)
            binding.image15minEscuro.setVisibility(View.VISIBLE)
            binding.image30minClaro.setVisibility(View.VISIBLE)
            binding.image30minEscuro.setVisibility(View.GONE)
            binding.image60minClaro.setVisibility(View.VISIBLE)
            binding.image60minEscuro.setVisibility(View.GONE)
            valor = 23.00F
            duracao = 15
        }

        binding.image30minClaro.setOnClickListener{
            binding.image15minClaro.setVisibility(View.VISIBLE)
            binding.image15minEscuro.setVisibility(View.GONE)
            binding.image30minClaro.setVisibility(View.GONE)
            binding.image30minEscuro.setVisibility(View.VISIBLE)
            binding.image60minClaro.setVisibility(View.VISIBLE)
            binding.image60minEscuro.setVisibility(View.GONE)
            valor  = 30.00F
            duracao = 30
        }

        binding.image60minClaro.setOnClickListener{
            binding.image15minClaro.setVisibility(View.VISIBLE)
            binding.image15minEscuro.setVisibility(View.GONE)
            binding.image30minClaro.setVisibility(View.VISIBLE)
            binding.image30minEscuro.setVisibility(View.GONE)
            binding.image60minClaro.setVisibility(View.GONE)
            binding.image60minEscuro.setVisibility(View.VISIBLE)
            valor = 39.90F
            duracao = 60
        }

        binding.imageRButtonAgoraFalse.setOnClickListener{
            binding.imageRButtonAgoraFalse.setVisibility(View.GONE)
            binding.imageRButtonAgoraTrue.setVisibility(View.VISIBLE)
            binding.imageRButtonAgendarFalse.setVisibility(View.VISIBLE)
            binding.imageRButtonAgendarTrue.setVisibility(View.GONE)
            binding.camposDataHora.setVisibility(View.GONE)
            data = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(LocalDateTime.now())
            horario = DateTimeFormatter.ofPattern("HH:mm").format(LocalDateTime.now())
        }

        binding.imageRButtonAgendarFalse.setOnClickListener{
            binding.imageRButtonAgoraFalse.setVisibility(View.VISIBLE)
            binding.imageRButtonAgoraTrue.setVisibility(View.GONE)
            binding.imageRButtonAgendarFalse.setVisibility(View.GONE)
            binding.imageRButtonAgendarTrue.setVisibility(View.VISIBLE)
            binding.camposDataHora.setVisibility(View.VISIBLE)
            data = binding.campoDataAgendar.text.toString()
            horario = binding.campoHorarioAgendar.text.toString()
        }


        binding.botaoConfirmar.setOnClickListener {
            val textEmail :String = "Olá!<br>Segue os detalhes do agendamento: <br><br>" +
                    "<b>Valor:</b> R$ ${String.format("%.2f", valor)} <br>" +
                    "<b>Duração:</b> $duracao minutos <br>" +
                    "<b>Data:</b> $data<br>" +
                    "<b>Horário:</b> $horario<br>" +
                    "<b>Passeador:</b> ${p.name}"
            Thread {
                EmailService.sendEmail("Confirmação de Agendamento",
                    "vladfarias@gmail.com",textEmail ,"Vladimir" )
            }.start()

            val it = Intent(this, TelaInicialActivity::class.java)
            startActivity(it)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home){
            finish()
        }
        return true
    }
}