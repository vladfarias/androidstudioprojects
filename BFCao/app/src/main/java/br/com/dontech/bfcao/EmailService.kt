package br.com.dontech.bfcao

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

object EmailService{
val host = "https://bfcao-api.herokuapp.com"
val TAG = "WS_BFCaoApp"

fun sendEmail(titulo : String, email:String, texto: String, nome:String) {
  val dados = "{" +
    "\"sender\":{" +
        "\"name\":\"BFCao\"," +
        "\"email\":\"bfcao.mobile@gmail.com\""+
    "}," +
    "\"to\":["+
    "{" +
        "\"email\":\"${email}\"," +
        "\"name\":\"${nome}\"" +
    "}" +
    "]," +
    "\"subject\":\"${titulo}\"," +
    "\"htmlContent\":\"<html><head></head><body><p>${texto}.</p></body></html>\"" +
"}"
    HttpHelper.postEmail("https://api.sendinblue.com/v3/smtp/email", dados)
}

inline fun <reified T>parseJson(json:String): T {
    val type = object : TypeToken<T>(){}.type
    return Gson().fromJson<T>(json, type)
}
}