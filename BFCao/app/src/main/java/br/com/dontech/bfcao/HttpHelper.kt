package br.com.dontech.bfcao

import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import java.io.IOException

object HttpHelper {

    val JSON = "application/json; charset=utf-8".toMediaTypeOrNull()

    // Construção das chamadas
    fun get(url: String): String{
        val request = Request.Builder().url(url).get().build()
        return getJson(request)
    }

    fun postEmail(url: String, json : String): String{
        var payLoad = RequestBody.create(JSON, json)
        val request = Request.Builder().url(url).header("accept","application/json")
            .addHeader("api-key","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")
            .addHeader("content-type","application/json" )
            .post(payLoad).build()
        return getJson(request)
    }

    fun post(url: String, json : String): String{

        var payLoad = RequestBody.create(JSON, json)
        val request = Request.Builder().url(url).post(payLoad).build()
        return getJson(request)
    }

    fun delete(url: String): String {
        val request = Request.Builder().url(url).delete().build()
        return getJson(request)
    }

    var client = OkHttpClient()

    // requisição Http
    private fun getJson(request: Request): String {
        val response = client.newCall(request).execute()
        val payLoad = response.body
        var json = payLoad!!.string()
        return json
    }
}