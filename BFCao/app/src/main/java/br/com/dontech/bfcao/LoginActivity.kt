package br.com.dontech.bfcao

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import br.com.dontech.bfcao.databinding.LoginBinding
import kotlin.concurrent.thread

class LoginActivity : AppCompatActivity() {

    private val binding by lazy {
        LoginBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val nomeUsuario = Prefs.getString("nomeUsuario");
        val senhaUsuario = Prefs.getString("senhaUsuario");
        val checkBox = Prefs.getBoolean("checkBox")

        binding.campoUsuario.setText(nomeUsuario)
        binding.campoSenha.setText(senhaUsuario)
        binding.checkBoxLogin.isChecked = checkBox

        binding.imageLogo.setImageResource(R.drawable.logo)

        binding.linkCadastrar.setOnClickListener {
            val intent = Intent(this, CadastroActivity:: class.java)
            startActivity(intent)

        }

        binding.linkRecuperarSenha.setOnClickListener {
            val i = Intent(this, EsqueciSenhaActivity:: class.java)
            startActivity(i)

        }

        binding.botaoLogin.setOnClickListener {
            val intent = Intent(this, TelaInicialActivity::class.java)
            if(binding.checkBoxLogin.isChecked){
                Prefs.setString("nomeUsuario", binding.campoUsuario.text.toString())
                Prefs.setString("senhaUsuario", binding.campoSenha.text.toString())
            } else {
                Prefs.setString("nomeUsuario", "")
                Prefs.setString("senhaUsuario", "")
            }
            Prefs.setBoolean("checkBox", binding.checkBoxLogin.isChecked)
            startActivity(intent)
        }


    }
}