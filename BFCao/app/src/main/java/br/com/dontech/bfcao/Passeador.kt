package br.com.dontech.bfcao

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "passeador")
class Passeador:Serializable {

    @PrimaryKey
    var id: Long? = null
    var name: String = ""
    var rate: Float = 5.0F
    var rides: Int? = null
    var photo: String = ""

}