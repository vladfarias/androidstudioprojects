package br.com.dontech.bfcao

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.dontech.bfcao.databinding.ActivityPasseadoresBinding
import java.util.Random


class PasseadorActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityPasseadoresBinding.inflate(layoutInflater)
    }

    private var passeadores = listOf<Passeador>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportActionBar?.title = "Passeadores"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.recyclerPasseadores?.layoutManager = LinearLayoutManager(this)
        binding.recyclerPasseadores?.setHasFixedSize(true)

    }

    override fun onResume() {
        super.onResume()
        this.taskPasseadores()

    }

    fun aleatorio(): Int{
        var nAleatorio = Random()
        return nAleatorio.nextInt(passeadores.size + 1)
    }

    private fun taskPasseadores(){

        Thread{
            passeadores = PasseadorService.getPasseadores()
            runOnUiThread{
                binding.recyclerPasseadores?.adapter = PasseadorAdapter(passeadores) {
                    onClickPasseador(it)
                }
                //enviar notificação
                val intent = Intent(this, DetalhesAgendamentoNowActivity::class.java)
                var indice = aleatorio()
                intent.putExtra("passeador", passeadores[indice])
                NotificationUtil.create(1, intent, "BFCao",
                "Você já chamou ${passeadores[indice].name} 🐕‍🦺 ♥ ?")

            }
        }.start()

        for (passeador: Passeador in passeadores ){
            DatabaseManeger.getPasseadorDAO().insert(passeador)
        }
    }

    fun onClickPasseador(passeador: Passeador){
        // Toast.makeText(this, "Clicou no Passeador", Toast.LENGTH_LONG).show()

        //mandar os dados do passeador para o Layout
        var it = Intent(this, DetalhesAgendamentoNowActivity:: class.java)
        it.putExtra("passeador", passeador)
        startActivity(it)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home){
            finish()
        }
        return true
    }

}
