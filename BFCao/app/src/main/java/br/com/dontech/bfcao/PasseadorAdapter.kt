package br.com.dontech.bfcao

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class PasseadorAdapter (
    val passeadores: List<Passeador>, val onClick: (Passeador)-> Unit
    ): RecyclerView.Adapter<PasseadorAdapter.PasseadorViewHolder>() {

        class PasseadorViewHolder(view: View): RecyclerView.ViewHolder(view){
            var cardImage: ImageView
            val cardNome: TextView
            var cardAvaliacao: TextView
            //var cardLocalizacao: TextView
            var cardView: CardView
            var cardBotao: Button

            init {
                cardView = view.findViewById(R.id.cardPasseadores)
                cardImage = view.findViewById(R.id.cardImage)
                cardNome = view.findViewById(R.id.cardNome)
                cardAvaliacao = view.findViewById(R.id.cardAvaliacao)
                //cardLocalizacao = view.findViewById(R.id.cardLocalizacao)
                cardBotao = view.findViewById(R.id.botao_selecionar)

            }
        }
    //Pega quantos itens existirá nesse recycleview
    override fun getItemCount() = this.passeadores.size

    // Qual layout é ultilizado para criar o card
    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int):
            PasseadorViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_passeadores,
            parent, false)

            return PasseadorViewHolder(view)
    }

    override fun onBindViewHolder(holder: PasseadorViewHolder, position: Int) {
        val passeador = this.passeadores[position]



        holder.cardView.visibility = View.VISIBLE
     // Pegar Imagem
        /*if (!passeador.photo.equals("")){
        Picasso.with(holder.itemView.context
        ).load(passeador.photo).fit().into(holder.cardImage,
            object: com.squareup.picasso.Callback{

                override fun onError() {
                    holder.cardView.visibility = View.GONE
                }

                override fun onSuccess() {
                    holder.cardView.visibility = View.GONE
                }
        })
        }*/
        Picasso.with(holder.itemView.context
        ).load(passeador.photo).fit().into(holder.cardImage,
            object: com.squareup.picasso.Callback{

                override fun onError() {

                }

                override fun onSuccess() {

                }
            })
        holder.cardNome.text = passeador.name
        holder.cardAvaliacao.text = passeador.rate.toString()
        holder.cardBotao.setOnClickListener {this.onClick(passeador)}
    }


}