package br.com.dontech.bfcao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PasseadorDAO {

    @Query("SELECT * FROM passeador where id = :id")
    fun getByID(id:Long):Passeador?

    @Query("SELECT * FROM passeador LIMIT 10" )
    fun findAll(): List<Passeador>

    // Insert into passeador(nome, avaliacao, localizacao...) values("", "", "")
    @Insert
    fun insert(passeador:Passeador)

    @Delete
    fun delete(passeador: Passeador)
}