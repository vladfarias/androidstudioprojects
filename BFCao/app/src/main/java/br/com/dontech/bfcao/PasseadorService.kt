package br.com.dontech.bfcao

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object PasseadorService {
    val host = "https://bfcao-api.herokuapp.com"
    val TAG = "WS_BFCaoApp"

    fun getPasseadores(): List<Passeador> {

        try {
            val url = "$host/api/v1/walker"
            val json = HttpHelper.get(url)
            var passeadores = parseJson<ReturnPasseadores>(json)
            Log.d(TAG, json)

            return passeadores?.data?.walkers!!

        } catch (ex: Exception){
            Log.e(TAG, ex.toString())
            var passeadores = DatabaseManeger.getPasseadorDAO().findAll()
            return passeadores
        }

    }

    //Identifica o tipo a ser transformado e transforma o json no tipo identificado
    inline fun <reified T>parseJson(json:String): T {
        val type = object : TypeToken<T>(){}.type
        return Gson().fromJson<T>(json, type)
    }
}