package br.com.dontech.bfcao
import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import br.com.dontech.bfcao.databinding.TelaInicialBinding
import com.google.android.material.navigation.NavigationView

class TelaInicialActivity : AppCompatActivity() , NavigationView.OnNavigationItemSelectedListener{

    private val binding by lazy {
        TelaInicialBinding.inflate(layoutInflater)
    }

    private var passeadores = listOf<Passeador>()

    private val context: Context get() = this
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)


        binding.botaoPasseadoresDisponiveis.setOnClickListener{
            val it = Intent(this, PasseadorActivity::class.java)
            startActivity(it)
        }

        // colocar toolbar
        setSupportActionBar(binding.toolbarInclude.toolbar)

        // alterar título da ActionBar
        supportActionBar?.title = "Bem-Vindo" + " Usuário"

        // up navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Configuração do menu lateral
        configuraMenuLateral()

    }

    // configuração do navigation Drawer com a toolbar
    private fun configuraMenuLateral() {
        // ícone de menu (hamburger) para mostrar o menu
        var toogle = ActionBarDrawerToggle(this, binding.layoutMenuLateral, binding.toolbarInclude.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)

        binding.layoutMenuLateral.addDrawerListener(toogle)
        toogle.syncState()

        binding.menuLateral.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_perfil -> {
                Toast.makeText(this, "Clicou Perfil", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_passeios -> {
                Toast.makeText(this, "Clicou Passeios", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_pets -> {
                Toast.makeText(this, "Clicou Forum", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_cartoes -> {
                Toast.makeText(this, "Clicou Localização", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_config -> {
                Toast.makeText(this, "Clicou Localização", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_ajuda -> {
                Toast.makeText(this, "Clicou Config", Toast.LENGTH_SHORT).show()
            }
        }
// fecha menu depois de tratar o evento
        binding.layoutMenuLateral.closeDrawer(GravityCompat.START)
        return true
    }

    fun cliqueSair() {
        val returnIntent = Intent();
        returnIntent.putExtra("result","Saída do BrewerApp");
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    // método sobrescrito para inflar o menu na Actionbar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // infla o menu com os botões da ActionBar
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if(id == R.id.action_buscar){
            Toast.makeText(this, "Buscar", Toast.LENGTH_SHORT).show()

        } else if (id == R.id.action_atualizar){
            Toast.makeText(this, "Atualizar", Toast.LENGTH_SHORT).show()

        } else if (id == R.id.action_config){

            val it =Intent(this, ConfiguracoesActivity::class.java)
            startActivity(it)
        } else if (id == R.id.action_atualizar_conta){

            val it =Intent(this, ConfiguracoesActivity::class.java)
            startActivity(it)
        } else if (id == android.R.id.home){
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}