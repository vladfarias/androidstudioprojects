package br.com.dontech.bfcao

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "user")
class User:Serializable {

    @PrimaryKey
    var id: Long? = null
    var name: String = ""
    var email: String = ""
    var password: String = ""
    var lastname: String= ""
    var zipcode: Int? = null
    var adress: String = ""
    var phone: Int? = null

}