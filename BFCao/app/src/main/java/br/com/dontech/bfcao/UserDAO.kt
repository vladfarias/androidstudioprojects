package br.com.dontech.bfcao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDAO {

    @Query("SELECT * FROM user where id = :id")
    fun getByID(id:Long):User?

    @Query("SELECT * FROM user" )
    fun findAll(): List<User>

    // Insert into passeador(nome, avaliacao, localizacao...) values("", "", "")
    @Insert
    fun insert(user: User)

    @Delete
    fun delete(user: User)
}