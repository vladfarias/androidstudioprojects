package br.com.dontech.bfcao

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

object UserService {

    val host = "https://bfcao-api.herokuapp.com/api/v1"
    val TAG = "WS_BFCaoApp"

    fun saveUser(user: User) {

        val json = GsonBuilder().create().toJson(user)
        val postData = HttpHelper.post("$host/user/", json)
        Log.d(TAG, postData)
    }

    inline fun <reified T>parseJson(json:String): T {
        val type = object : TypeToken<T>(){}.type
        return Gson().fromJson<T>(json, type)
    }
}


